package nl.utwente.di.temperatureConverter;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class TemperatureConverter extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TempConverter tempConverter;

    public void init() throws ServletException {
        tempConverter = new TempConverter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter responseWriter = response.getWriter();
        StringBuilder outputPage = new StringBuilder();
        final String title = "Celsius to Fahrenheit Converter";

        String param = request.getParameter("temperatureInput");

        // If the parameter is set and is a valid input
        if (param != null && tempConverter.isValidInput(param)) {
            outputPage.append("<!DOCTYPE html>\n<html>\n<head>\n");
            outputPage.append("<title>" + title + " - Result</title>\n");
            outputPage.append("<link rel=stylesheet href=\"style.css\">\n");
            outputPage.append("</head>\n<body>\n");
            outputPage.append("<h1>" + title + "</h1>\n");
            outputPage.append("<p>" + param + "°C is <b>");
            outputPage.append(tempConverter.toFahrenheit(param) + "°F</b></p>\n");
            outputPage.append("</body>\n</html>");
            responseWriter.write(outputPage.toString());
            return;
        }

        // Error page
        outputPage.append("<!DOCTYPE html>\n<html>\n<head>\n");
        outputPage.append("<title>" + title + " - Error</title>\n");
        outputPage.append("<link rel=stylesheet href=\"style.css\">\n");
        outputPage.append("</head>\n<body>\n");
        outputPage.append("<h1>" + title + "</h1>\n");
        outputPage.append("<p style=\"color: red;\">The given input was invalid and " +
                "could not be converted to Fahrenheit</p>\n");
        outputPage.append("</body>\n</html>");
        responseWriter.write(outputPage.toString());
    }
}
