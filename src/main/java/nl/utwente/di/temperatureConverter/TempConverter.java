package nl.utwente.di.temperatureConverter;

/**
 * Methods associated with the TemperatureConverter Servlet.
 * @author Matthijs Veldkamp
 */
public class TempConverter {
    /**
     * Check if a given parameter is valid for the other method in this class.
     * @param input The parameter received.
     * @return <code>true</code> if the parameter can be converted to Fahrenheit
     */
    public boolean isValidInput(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Turn a given decimal number in celsius into Fahrenheit. The given string should be convertable to a double.
     * @param input The value to convert.
     * @return The value's Fahrenheit value.
     */
    public double toFahrenheit(String input) {
        double inputValue;
        try {
            inputValue = Double.parseDouble(input);
        } catch (NumberFormatException e) {
            return 0.0;
        }
        return (inputValue * 1.8) + 32;
    }
}
